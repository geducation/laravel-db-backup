<?php

return array(

    'databases' => array(

        'remote' => array(
            'host'     => env('BACKUP_DB__DB_HOST', env('DB_HOST', 'localhost')),
            'name'     => env('BACKUP_DB__DB_NAME', env('DB_DATABASE', 'localhost')),
            'username' => env('BACKUP_DB__DB_USERNAME', env('DB_USERNAME', 'homestead')),
            'password' => env('BACKUP_DB__DB_PASSWORD', env('DB_PASSWORD', 'secret')),
        ),

        'local' => array(
            'host'     => env('DB_HOST', 'localhost'),
            'name'     => env('DB_DATABASE', 'localhost'),
            'username' => env('DB_USERNAME', 'homestead'),
            'password' => env('DB_PASSWORD', 'secret'),
        ),

    ),

    /*
    |--------------------------------------------------------------------------
    | Remote Server Connections
    |--------------------------------------------------------------------------
    |
    | These are the servers that will be accessible via the Remote class.
    | Each server is identified by a key and necessary authentication options:
    |
    | - host
    | - port
    | - username
    | - key and keyphrase if you are SSH'ing using authorized keys
    | - password if you're using a password.
    |
    | Note: login with key has higher priority. Which means, Remote will only
    | attempt to log in via username/password if key is empty.
    |
    */

    'remote-server' => array(
        'host'              => env('BACKUP_DB__SSH_HOST', 'localhost'),
        'port'              => env('BACKUP_DB__SSH_PORT', 22),
        'username'          => env('BACKUP_DB__SSH_USERNAME', 'forge'),
        'key'               => env('BACKUP_DB__SSH_KEY_PATH', '/home/vagrant/.ssh/id_rsa'),
        'ask-for-keyphrase' => env('BACKUP_DB__SSH_USE_KEYPHRASE', false),
    ),

);