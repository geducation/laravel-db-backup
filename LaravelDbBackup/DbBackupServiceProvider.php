<?php

namespace Geducation\LaravelDbBackup;

use Geducation\LaravelDbBackup\Console\RunCommand;
use Illuminate\Support\ServiceProvider;

class DbBackupServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $name = 'db-backup';
        $packageConfigPath = __DIR__ . "/config/{$name}.php";

        $this->publishes([
            $packageConfigPath => config_path("{$name}.php"),
        ]);

        $this->mergeConfigFrom(
            $packageConfigPath, $name
        );

        $this->app->singleton(
            "command.{$name}.run",
            function ($app) {
                return new RunCommand();
            }
        );

        $this->commands(
            "command.{$name}.run"
        );
    }
}
