<?php

namespace Geducation\LaravelDbBackup\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use PhanAn\Remote\Remote;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Process\Process;

class RunCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db-backup:ssh {--exclude=revisions} {--ask-for-keyphrase}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy a database from a remote machine via SSH to the local Laravel app.';

    /**
     * @var Remote
     */
    protected $connection;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $exclude = $this->option('exclude');

        if ($exclude === 'false') {
            $dumpFullDatabase = true;
        } else {
            $dumpFullDatabase = false;
            $tablesToIgnore = explode(',', $exclude);
        }

        $sshConfig = config('db-backup.remote-server', []);

        if ($this->option('ask-for-keyphrase') || config('db-backup.remote-server.ask-for-keyphrase') == true) {
            $keyphrase = $this->askForKeyphrase();
            if ($keyphrase) {
                $sshConfig['keyphrase'] = $keyphrase;
            }
        }

        $this->connection = new Remote($sshConfig);

        $timestamp = date('Y-m-d_H.i.s');
        $remoteDatabase = config('db-backup.databases.remote.name');
        $localDatabase = config('db-backup.databases.local.name');
        $filename = "{$remoteDatabase}.{$timestamp}.sql.gz";

        if (!file_exists('storage/db')) {
            mkdir('storage/db', 0777);
        }

        $localSqlDumpPath = "storage/db/{$filename}";
        $localDbUsername = config('db-backup.databases.local.username');
        $localDbPassword = config('db-backup.databases.local.password');
        $localDbHost = config('db-backup.databases.local.host');

        $localMysqlConnect = "mysql -u {$localDbUsername} -p{$localDbPassword} -h {$localDbHost} --port 3306";

        $remoteGzip = "cat export.sql | gzip > {$filename}";

        $localDrop = "{$localMysqlConnect} -e \"DROP DATABASE {$localDatabase}\";";
        $localCreate = "{$localMysqlConnect} -e \"CREATE DATABASE {$localDatabase}\";";
        $localRestore = "gzip -d < {$localSqlDumpPath} | {$localMysqlConnect} {$localDatabase}";

        if ($dumpFullDatabase) {
            $remoteMysqlDump = "mysqldump {$remoteDatabase} | gzip >  {$filename}";

            $this->info('Dumping FULL database');
            $this->execRemote($remoteMysqlDump);
        } else {
            $ignore = [];
            foreach ($tablesToIgnore as $table) {
                $ignore[] = "--ignore-table={$remoteDatabase}.{$table}";
            }

            $remoteMysqlDumpStructure = "mysqldump --no-data {$remoteDatabase} > export.sql";
            $remoteMysqlDumpData = "mysqldump --no-create-info " . implode(' ', $ignore) . " {$remoteDatabase} >> export.sql";

            $this->info('Dumping PARTIAL database (EXCLUDING ' . implode(', ', $tablesToIgnore) . ' table data)');
            $this->execRemote($remoteMysqlDumpStructure);

            $this->info($remoteMysqlDumpData);
            $this->execRemote($remoteMysqlDumpData);

            $this->info($remoteGzip);
            $this->execRemote($remoteGzip);
        }

        $this->info('Copying resulting file to local directory');
        $this->connection->getConnection()->get($filename, $localSqlDumpPath);

        $this->info('Deleting from server file');
        $this->connection->getConnection()->delete('export.sql');
        $this->connection->getConnection()->delete($filename);

        $this->info('Dropping existing local DB');
        $this->execLocal($localDrop);

        $this->info('Creating new local DB');
        $this->execLocal($localCreate);

        $this->info('Importing SQL');
        $this->execLocal($localRestore);
    }

    private function execLocal($command)
    {
        $process = new Process($command);
        $process->setTimeout(null);
        $process->run();
    }

    private function execRemote($command)
    {
        $this->connection->getConnection()->exec($command);
    }

    private function askForKeyphrase()
    {
        $question = new Question("Enter your SSH key's passphrase (if any)");
        $question->setHidden(true);
        $question->setValidator(function ($value) {
            return $value;
        });

        return $this->output->askQuestion($question);
    }

    public function info($string, $verbosity = null)
    {
        $string = Carbon::now() . ': ' . $string;
        parent::info($string, $verbosity);
    }
}
